defmodule ANDS.CollectionTest do
  use ExUnit.Case
  doctest ANDS.Collection

  # These tests are a bit simple-minded, but they should suffice to alert us
  # if some XML code breaks.

  test "Format a single record" do
    input = %{
      collection_id: 5,
      created_utc: ~N[2017-11-30 23:04:22.229816],
      editors: [],
      id: 5,
      in_solr: false,
      is_public: true,
      modified_utc: ~N[2017-12-05 00:29:09.036488],
      name: "¡Cinco!",
      owners: [
        %{"id" => 5, "name" => "User Five"},
        %{"id" => 2, "name" => "User Two"}
      ],
      record_count: 6,
      synopsis: "Five!",
      viewers: [%{"id" => 3, "name" => "User Three"}],
    }

    output = String.trim("""
<registryObject group="Deakin University">
	<key>https://huni.net.au/#/collection/5</key>
	<originatingSource>https://huni.net.au</originatingSource>
	<collection type="collection">
		<name>
			<namePart>¡Cinco!</namePart>
		</name>
		<description type="brief">This is a public collection of records relating to this topic from the HuNI Virtual Laboratory.

HuNI records fall into six different categories: Person, Organisation, Work, Place, Concept and Event. They are derived from records in the contributing datasets which are aggregated and assembled to form the HuNI Virtual Laboratory.

This HuNI public collection has been assembled by a HuNI user for research and study purposes.
</description>
		<rights>
			<licence rightsUri="https://huni.net.au/#/data-policy" type="CC-BY-NC-SA">HuNI Data Policy</licence>
			<accessRights rightsUri="https://huni.net.au/#/data-policy" type="open">HuNI Data Policy</accessRights>
		</rights>
		<dates type="dc.created">
			<date dateFormat="W3CDTF" type="dateFrom">2017-11-30T23:04:22Z</date>
		</dates>
		<dates type="dc.issued">
			<date dateFormat="W3CDTF" type="dateFrom">2017-12-05T00:29:09Z</date>
		</dates>
		<location>
			<address>
				<electronic target="landingPage" type="url">
					<value>https://huni.net.au/#/collection/5</value>
				</electronic>
			</address>
		</location>
		<coverage>
			<spatial type="iso31661">AU</spatial>
		</coverage>
		<relatedInfo type="party">
			<title>User Five</title>
			<identifier type="uri">https://huni.net.au/#/user/5</identifier>
			<relation type="isManagedBy"/>
		</relatedInfo>
		<relatedInfo type="party">
			<title>User Two</title>
			<identifier type="uri">https://huni.net.au/#/user/2</identifier>
			<relation type="isManagedBy"/>
		</relatedInfo>
		<relatedObject>
			<key>NXcBV96UxPUfYe6Mt6JNuX6049o9WUfAIh1tgXdDDYIr2F4lCDZX</key>
			<relation type="isProducedBy"/>
		</relatedObject>
		<subject type="anzsrc-for">210303</subject>
		<citationInfo>
			<citationMetadata>
				<identifier type="uri">https://huni.net.au/#/collection/5</identifier>
				<title>¡Cinco!</title>
				<publisher>HuNI Virtual Laboratory</publisher>
				<url>https://huni.net.au/#/collection/5</url>
				<contributor>
					<namePart>User Five</namePart>
				</contributor>
				<contributor>
					<namePart>User Two</namePart>
				</contributor>
				<date type="publicationDate">2017-11-30T23:04:22Z</date>
			</citationMetadata>
		</citationInfo>
	</collection>
</registryObject>
""")
    assert ANDS.Collection.formatCollection(input) == output
  end
end
