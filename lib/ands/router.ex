defmodule ANDS.Router do
  use Plug.Router
  require Logger

  plug :match
  plug :dispatch

  @ets_table  :ands
  @db_key     "db"

  def init(_opts) do
    initDatabase()
  end

  get "/ping" do
    conn
      |> put_resp_content_type("text/plain")
      |> send_resp(200, "OK")
  end

  get "/version.json" do
    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, %{ git: System.get_env("GIT_SHA") } |> Poison.encode!)
  end

  get "/" do
    db = getDatabase()

    ANDS.Database.listCollections(db, fn collectionRows ->
      collectionRows
        |> ANDS.Collection.formatXML
        |> streamInto(chunkedConnection(conn, "application/xml"))
    end)
  end

  match _, do: send_resp(conn, 404, "Not found")

  defp initDatabase do
    db = ANDS.Database.connect
    :ets.new(@ets_table, [:named_table])
    :ets.insert(@ets_table, {@db_key, db})
  end

  defp getDatabase do
    [ {_key, db} ] = :ets.lookup(@ets_table, @db_key)
    db
  end

  # Set a connection into chunked mode.
  defp chunkedConnection(conn, content_type) do
    conn
      |> put_resp_content_type(content_type)
      |> send_chunked(200)
  end

  # Take a data stream, and feed it into the chunked mode connection.
  defp streamInto(dataStream, connection) do
    addChunk! = fn(data, connection) ->
      { :ok, nextConnection } = Plug.Conn.chunk(connection, data)
      nextConnection
    end
    Enum.reduce(dataStream, connection, addChunk!)
  end

end
