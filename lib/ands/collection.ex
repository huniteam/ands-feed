defmodule ANDS.Collection do

  @prefix """
<?xml version="1.0" encoding="UTF-8"?>
<registryObjects xmlns="http://ands.org.au/standards/rif-cs/registryObjects" xmlns:extRif="http://ands.org.au/standards/rif-cs/extendedRegistryObjects" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ands.org.au/standards/rif-cs/registryObjects http://services.ands.org.au/documentation/rifcs/schema/registryObjects.xsd">
"""

  @suffix """
</registryObjects>
"""

  @description """
This is a public collection of records relating to this topic from the HuNI Virtual Laboratory.

HuNI records fall into six different categories: Person, Organisation, Work, Place, Concept and Event. They are derived from records in the contributing datasets which are aggregated and assembled to form the HuNI Virtual Laboratory.

This HuNI public collection has been assembled by a HuNI user for research and study purposes.
"""

  def formatXML(collectionRows) do
      collectionRows
        |> Stream.map(&formatCollection/1)
        |> wrap(@prefix, @suffix)
  end

  # Made this public for ease of testing
  def formatCollection(collection) do
    #IO.inspect(collection)
    uri = "https://huni.net.au/#/collection/#{ collection.id }"
    {:registryObject, %{ group: "Deakin University" }, [
      {:key, nil, uri},
      {:originatingSource, nil, "https://huni.net.au"},
      {:collection, %{ type: "collection" }, [
        #{:identifier, %{ type: "uri" }, uri},
        {:name, nil, [
          {:namePart, nil, collection.name},
        ]},
        {:description, %{ type: "brief" }, @description },
        {:rights, nil, [
          {:licence, %{
              type: "CC-BY-NC-SA",
              rightsUri: "https://huni.net.au/#/data-policy"
            }, "HuNI Data Policy"
          },
          {:accessRights, %{
              type: "open",
              rightsUri: "https://huni.net.au/#/data-policy"
            }, "HuNI Data Policy"
          },
        ]},
        {:dates, %{ type: "dc.created" }, [
          {:date, %{ type: "dateFrom", dateFormat: "W3CDTF" },
            formatTimestamp(collection.created_utc)},
        ]},
        {:dates, %{ type: "dc.issued" }, [
          {:date, %{ type: "dateFrom", dateFormat: "W3CDTF" },
            formatTimestamp(collection.modified_utc)},
        ]},
        {:location, nil, [
          {:address, nil, [
            {:electronic, %{ type: "url", target: "landingPage" }, [
              {:value, nil, uri},
            ]},
          ]},
        ]},
        {:coverage, nil, [
          {:spatial, %{ type: "iso31661" }, "AU"},
        ]},
        Enum.map(collection.owners, &formatRelatedInfo/1),
        {:relatedObject, nil, [
          {:key, nil, "NXcBV96UxPUfYe6Mt6JNuX6049o9WUfAIh1tgXdDDYIr2F4lCDZX"},
          {:relation, %{ type: "isProducedBy" }, [ ]},
        ]},
        {:subject, %{ type: "anzsrc-for" }, 210303},
        {:citationInfo, nil, [
          {:citationMetadata, nil, [
            {:identifier, %{ type: "uri" }, uri},
            {:title, nil, collection.name},
            {:publisher, nil, "HuNI Virtual Laboratory"},
            {:url, nil, uri},
            Enum.map(collection.owners, fn owner ->
              {:contributor, nil, [
                {:namePart, nil, owner["name"]},
              ]}
            end),
            {:date, %{ type: "publicationDate" },
              formatTimestamp(collection.created_utc)},
          ]},
        ]},
      ]},
    ]} |> XmlBuilder.generate
  end

  defp wrap(stream, prefix, suffix) do
    Stream.concat([ [prefix], stream, [suffix] ])
  end

  defp formatTimestamp(ts) do
    dd = fn(num) ->
      Integer.to_string(num) |> String.pad_leading(2, "0")
    end
    date = "#{ ts.year }-#{ dd.(ts.month) }-#{ dd.(ts.day) }"
    time = [ ts.hour, ts.minute, ts.second ] |> Enum.map(dd) |> Enum.join(":")
    "#{ date }T#{ time }Z"
  end

  defp formatRelatedInfo(owner) do
    name = owner["name"]
    id   = owner["id"]
    uri  = "https://huni.net.au/#/user/#{ id }"
    {:relatedInfo, %{ type: "party" }, [
      {:title, nil, name},
      {:identifier, %{ type: "uri" }, uri},
      {:relation, %{ type: "isManagedBy" }, [ ]},
    ]}
  end

end
