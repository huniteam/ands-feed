defmodule ANDS.Database do
  # Install a postgrex handler for json columns.
  Postgrex.Types.define(ANDS.Database.PostgrexTypes, [], json: Poison)

  def connect do
    { :ok, conn } =
      Postgrex.start_link(pool: DBConnection.Poolboy,
                          name: :"#{__MODULE__}_Poolboy",
                          pool_size: 8,
                          types: ANDS.Database.PostgrexTypes)
    conn
  end

  @collectionsQuery """
    SELECT *
      FROM huni.collection c
      JOIN huni.collection_extra e
        ON c.id = e.collection_id
     WHERE c.is_public
     ORDER BY id ASC
  """

  @opts [ pool: DBConnection.Poolboy ]

  def listCollections(conn, collector) do
    streamingQuery(conn, @collectionsQuery, [ max_rows: 100 ], collector)
  end

  # Postgrex.stream behaves in a monadic fashion. It returns a stream, but the
  # stream needs to be consumed within the transaction - we can't return the
  # stream. That's why we pass in the collector function.
  defp streamingQuery(conn, query, options, collector) do
    { :ok, result } = Postgrex.transaction(conn, fn conn ->
      query = Postgrex.prepare!(conn, "", query, @opts)
      Postgrex.stream(conn, query, [], options ++ @opts)
        |> rows
        |> collector.()
    end, @opts)
    result
  end

  # Postgrex.stream returns a stream where each item contains N rows. I'd
  # prefer just a stream of rows, so we use flat_map here to do that.
  defp rows(stream) do
    Stream.flat_map(stream, fn res -> makeObjects(res.columns, res.rows) end)
  end

  # The row chunks have a single header array, followed by a list of row value
  # arrays. Here we convert that to an array of standalone maps.
  defp makeObjects(columns, rows) do
    Enum.map(rows, fn values -> makeObject(columns, values) end)
  end

  defp makeObject(columns, values) do
    Enum.map(columns, &String.to_atom/1)
      |> Enum.zip(values)
      |> Map.new
  end

end
