defmodule ANDS do
  use Application
  require Logger

  def start(_type, _args) do
    children = [
      Plug.Adapters.Cowboy.child_spec(:http, ANDS.Router, [], port: 4000),
    ]

    Logger.info "ANDS Feed Started"

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
