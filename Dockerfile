FROM elixir:1.6.1

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
      inotify-tools

WORKDIR /app

RUN mix local.hex --force \
 && mix local.rebar --force

COPY mix.exs /app

RUN mix deps.get --force \
 && mix compile

ENV MIX_ENV=prod

COPY . /app

RUN mix compile

ARG GIT_SHA="(Missing - development version?)"
ENV GIT_SHA=$GIT_SHA

CMD ["./start.sh"]
