defmodule ANDS.Mixfile do
  use Mix.Project

  def project do
    [
      app: :huni_feed_ands,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: applications(Mix.env),
      mod: {ANDS, []},
    ]
  end

  defp applications(:dev), do: applications(:all) ++ [:remix]
  defp applications(_all), do: [:db_connection, :logger]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy,       "~> 1.1.2"},
      {:plug,         "~> 1.4.3"},
      {:poison,       "~> 3.1"},
      {:poolboy,      "~> 1.5.1"},
      {:postgrex,     "~> 0.13.3"},
      {:remix,        "~> 0.0.1", only: :dev},
      {:xml_builder,  "~> 0.1.2"},
    ]
  end
end
